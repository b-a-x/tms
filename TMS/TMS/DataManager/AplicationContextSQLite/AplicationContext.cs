﻿using DataModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace TMS.DataManager.AplicationContextSQLite
{
    /// <summary>
    /// Контекст для работы с SQLite
    /// </summary>
    public class AplicationContext : DbContext, IAplicationContext
    {
        private const string _dbName = "Filename=TMSDev.db";

        public AplicationContext()
        {
            //TODO: Миграция (для упрощеной миграции нужно удалить бд)
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(_dbName);
        }

        public DbSet<User> Users { get; set; }
    }
}
