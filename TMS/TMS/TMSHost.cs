﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TMS
{
    public class TMSHost
    {
        private const string _appSettingsJson = "Config/appsettings.json";

        public TMSHost()
        {
        }

        protected virtual IConfigurationBuilder CreateConfigurationBuilder(string[] args)
        {
            return new ConfigurationBuilder()
                   .AddCommandLine(args)
                   .AddJsonFile(_appSettingsJson, false, true);
        }

        protected virtual IWebHostBuilder CreateWebHostBuilder(IConfigurationRoot configuration)
        {
            return new WebHostBuilder()
                   .UseKestrel(options => { })
                   .UseConfiguration(configuration)
                   .UseStartup<TMSStartup>();
        }

        public void Run(string[] args)
        {
            IConfigurationRoot configuration = CreateConfigurationBuilder(args).Build();

            CreateWebHostBuilder(configuration)
                .Build()
                .Run();
        }
    }
}
